package com.example.yaweather.main


import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * City represents the city weather fetched from the network
 */
@Entity
data class City constructor(
    val city_name: String,
    val temperature: Double,
    val condition: String,
    @PrimaryKey val id: String = "DZ-16")

/***
 * Very small database that will hold one City
 */
@Dao
interface CityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCity(City: City)

    @get:Query("select * from City where id = 'DZ-16'")
    val CityLiveData: LiveData<City?>
}

/**
 * CityDatabase provides a reference to the dao to repositories
 */
@Database(entities = [City::class], version = 1, exportSchema = false)
abstract class CityDatabase : RoomDatabase() {
    abstract val cityDao: CityDao
}

private lateinit var INSTANCE: CityDatabase

/**
 * Instantiate a database from a context.
 */
fun getDatabase(context: Context): CityDatabase {
    synchronized(CityDatabase::class) {
        if (!::INSTANCE.isInitialized) {
            INSTANCE = Room
                .databaseBuilder(
                    context.applicationContext,
                    CityDatabase::class.java,
                    "Cities_db"
                )
                .fallbackToDestructiveMigration()
                .build()
        }
    }
    return INSTANCE
}
