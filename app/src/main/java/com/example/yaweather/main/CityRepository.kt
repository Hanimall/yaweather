/*
 * Copyright (C) 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.yaweather.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.example.android.kotlincoroutines.util.BACKGROUND
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

/**
 * CityRepository provides an interface to fetch a City or request a new one be generated.
 *
 * Repository modules handle data operations. They provide a clean API so that the rest of the app
 * can retrieve this data easily. They know where to get the data from and what API calls to make
 * when data is updated. You can consider repositories to be mediators between different data
 * sources, in our case it mediates between a network API and an offline database cache.
 */
class CityRepository(val network: MainNetwork, val CityDao: CityDao) {

    /**
     * [LiveData] to load City.
     *
     * This is the main interface for loading a City. The City will be loaded from the offline
     * cache.
     *
     * Observing this will not cause the City to be refreshed, use [CityRepository.refreshCityWithCallbacks]
     * to refresh the City.
     */
    val City: LiveData<City?> = CityDao.CityLiveData.map { it }


    suspend fun refreshCity() {
        try {
            // Make network request using a blocking call
            val result = network.fetchCity()
            CityDao.insertCity(City(result.city_name, result.temperature.toDouble(), result.condition))
        } catch (cause: Throwable) {
            println(cause)
            // If anything throws an exception, inform the caller
            throw CityRefreshError("Unable to refresh City", cause)
        }
    }

}

/**
 * Thrown when there was a error fetching a new City
 *
 * @property message user ready error message
 * @property cause the original cause of this exception
 */
class CityRefreshError(message: String, cause: Throwable?) : Throwable(message, cause)

interface CityRefreshCallback {
    fun onCompleted()
    fun onError(cause: Throwable)
}
