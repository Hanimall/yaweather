package com.example.yaweather.main


import com.example.yaweather.model.City
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

private val service: MainNetwork by lazy {

    val okHttpClient = OkHttpClient.Builder()
        .build()

    val retrofit = Retrofit.Builder()
        .baseUrl("https://virtserver.swaggerhub.com/")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    retrofit.create(MainNetwork::class.java)
}

fun getNetworkService() = service

/**
 * Main network interface which will fetch a new welcome title for us
 */
interface MainNetwork {
    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("ya-ios/Yassir-Weather/1.0.0/forecast/DZ-16")
    suspend fun fetchCity(): City
}

