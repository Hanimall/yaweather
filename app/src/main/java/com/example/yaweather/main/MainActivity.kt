package com.example.yaweather.main

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.example.yaweather.R
import com.google.android.material.snackbar.Snackbar

/**
 * Show layout.activity_main and setup data binding.
 */
class MainActivity : AppCompatActivity() {

    /**
     * Inflate layout.activity_main and setup data binding.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val rootLayout: ConstraintLayout = findViewById(R.id.rootlayout)
        val cityName: TextView = findViewById(R.id.city_name)
        val temperature: TextView = findViewById(R.id.temperature)
        val condition: ImageView = findViewById(R.id.condition)
        val spinner: ProgressBar = findViewById(R.id.spinner)

        // Get MainViewModel by passing a database to the factory
        val database = getDatabase(this)
        val repository = CityRepository(getNetworkService(), database.cityDao)
        val viewModel = ViewModelProviders
            .of(this, MainViewModel.FACTORY(repository))
            .get(MainViewModel::class.java)

        // When rootLayout is clicked call onMainViewClicked in ViewModel
        rootLayout.setOnClickListener {
            viewModel.onMainViewClicked()
        }

        // update the city weather when the [MainViewModel.city] changes
        viewModel.city.observe(this) { value ->
            value?.let {
                println(it)
                cityName.text = it.city_name
                temperature.text = it.temperature.toString()
                when (it.condition) {
                    "SUNNY" -> condition.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_sunny) )
                    "CLOUDY" -> condition.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cloudy) )
                    "RAINY" -> condition.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_rainy) )
                    else -> { // Note the block
                        print("x is neither 1 nor 2")
                    }
                }
            }
        }

        // show the spinner when [MainViewModel.spinner] is true
        viewModel.spinner.observe(this) { value ->
            value.let { show ->
                spinner.visibility = if (show) View.VISIBLE else View.GONE
            }
        }

        // Show a snackbar whenever the [ViewModel.snackbar] is updated a non-null value
        viewModel.snackbar.observe(this) { text ->
            text?.let {
                Snackbar.make(rootLayout, text, Snackbar.LENGTH_SHORT).show()
                viewModel.onSnackbarShown()
            }
        }
    }
}
