package com.example.yaweather.model

class City(
    val city_name: String,
    val temperature: Int,
    val condition: String)